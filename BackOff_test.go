package policy

import (
	"context"
	"testing"
	"time"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestBackOffFailure(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(3)
	policy := NewBackOff(2, time.Millisecond*50)

	// Act.
	bt := time.Now()
	err := policy.Attempt(context.Background(), failable)
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("error 3"))
	test.That(t, dt < time.Millisecond*155, is.True())
	test.That(t, dt > time.Millisecond*145, is.True())
}

func TestBackOffSuccess(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(2)
	policy := NewBackOff(2, time.Millisecond*50)

	// Act.
	bt := time.Now()
	err := policy.Attempt(context.Background(), failable)
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, dt < time.Millisecond*155, is.True())
	test.That(t, dt > time.Millisecond*145, is.True())
}

func TestBackOffSuccessLonger(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(5)
	policy := NewBackOff(5, time.Millisecond*50)

	// Act.
	bt := time.Now()
	err := policy.Attempt(context.Background(), failable)
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, dt < time.Millisecond*1555, is.True())
	test.That(t, dt > time.Millisecond*1545, is.True())
}

func TestBackOffContextDeadline(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(1)
	policy := NewBackOff(1, time.Millisecond*50)
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*25)

	// Act.
	bt := time.Now()
	err := policy.Attempt(ctx, failable)
	cancel()
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err, is.EqualTo(context.DeadlineExceeded))
	test.That(t, dt < time.Millisecond*35, is.True())
	test.That(t, dt > time.Millisecond*15, is.True())
}
