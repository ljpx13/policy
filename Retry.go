package policy

import (
	"context"
	"time"
)

// Retry implements a basic retry policy with a maximum retry count and sleep
// duration.
type Retry struct {
	maximumRetryCount uint
	sleepDuration     time.Duration
}

var _ Policy = &Retry{}

// NewRetry creates a new Retry policy with the provided maximum retry
// count and sleep duration.  A maximum retry count of 0 means the Failable will
// be attempted a single time and the error returned directly, if any.
func NewRetry(maximumRetryCount uint, sleepDuration time.Duration) *Retry {
	return &Retry{
		maximumRetryCount: maximumRetryCount,
		sleepDuration:     sleepDuration,
	}
}

// Attempt attempts the provided Failable using the rules provided to the Retry
// policy.
func (p *Retry) Attempt(ctx context.Context, action Failable) error {
	c := uint(0)

	for {
		c++

		err := action()
		if err == nil || c > p.maximumRetryCount {
			return err
		}

		err = func() error {
			ticker := time.NewTicker(p.sleepDuration)
			defer ticker.Stop()

			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-ticker.C:
				return nil
			}
		}()

		if err != nil {
			return err
		}
	}
}
