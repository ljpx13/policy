package policy

import (
	"context"
	"testing"
	"time"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestRetryFailure(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(2)
	policy := NewRetry(1, time.Millisecond*50)

	// Act.
	bt := time.Now()
	err := policy.Attempt(context.Background(), failable)
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("error 2"))
	test.That(t, dt < time.Millisecond*55, is.True())
	test.That(t, dt > time.Millisecond*45, is.True())
}

func TestRetrySuccess(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(1)
	policy := NewRetry(1, time.Millisecond*50)

	// Act.
	bt := time.Now()
	err := policy.Attempt(context.Background(), failable)
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, dt < time.Millisecond*55, is.True())
	test.That(t, dt > time.Millisecond*45, is.True())
}

func TestRetryContextDeadline(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(1)
	policy := NewRetry(1, time.Millisecond*50)
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*25)

	// Act.
	bt := time.Now()
	err := policy.Attempt(ctx, failable)
	cancel()
	dt := time.Now().Sub(bt)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err, is.EqualTo(context.DeadlineExceeded))
	test.That(t, dt < time.Millisecond*35, is.True())
	test.That(t, dt > time.Millisecond*15, is.True())
}
