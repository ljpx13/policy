![](./icon.png)

# policy

Package `policy` implements some simple policies for retrying operations that
fail.
