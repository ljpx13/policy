package policy

import (
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestTestFailable(t *testing.T) {
	// Arrange.
	failable := NewTestFailable(2)

	// Act.
	err1 := failable()
	err2 := failable()
	err3 := failable()

	// Assert.
	test.That(t, err1, is.NotNil())
	test.That(t, err1.Error(), is.EqualTo("error 1"))

	test.That(t, err2, is.NotNil())
	test.That(t, err2.Error(), is.EqualTo("error 2"))

	test.That(t, err3, is.Nil())
}
