package policy

import "context"

// Policy defines the methods that any policy must implement.  Implementations
// must be able to accept a Failable that the policy will then attempt to
// complete.
//
// Implementations define their own retry/alerting/monitoring behaviour and must
// be thread-safe to allow policy reuse.
type Policy interface {
	Attempt(ctx context.Context, action Failable) error
}
