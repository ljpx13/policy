package policy

import "fmt"

// NewTestFailable returns a Failable that will succeed after the provided
// number of attempts.  Passing a value of 0 will cause the Failable to succeed
// on the first call.
func NewTestFailable(succeedAfter uint) Failable {
	c := uint(0)

	return func() error {
		if c == succeedAfter {
			return nil
		}

		c++
		return fmt.Errorf("error %v", c)
	}
}
