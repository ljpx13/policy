package policy

import (
	"context"
	"time"
)

// BackOff implements a retry policy with exponential back-off.
type BackOff struct {
	maximumRetryCount    uint
	initialSleepDuration time.Duration
}

var _ Policy = &BackOff{}

// NewBackOff creates a new BackOff policy with the provided maximum retry
// count and initial sleep duration.  Sleep duration is doubled for each
// subsequent attempt to complete the action.  A maximum retry count of 0 means
// the Failable will be attempted a single time and the error returned directly,
// if any.
func NewBackOff(maximumRetryCount uint, initialSleepDuration time.Duration) *BackOff {
	return &BackOff{
		maximumRetryCount:    maximumRetryCount,
		initialSleepDuration: initialSleepDuration,
	}
}

// Attempt attempts the provided Failable using the rules provided to the retry
// policy.
func (p *BackOff) Attempt(ctx context.Context, action Failable) error {
	c := uint(0)
	sleepDuration := p.initialSleepDuration

	for {
		c++

		err := action()
		if err == nil || c > p.maximumRetryCount {
			return err
		}

		err = func() error {
			ticker := time.NewTicker(sleepDuration)
			defer ticker.Stop()

			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-ticker.C:
				return nil
			}
		}()

		if err != nil {
			return err
		}

		sleepDuration = sleepDuration * 2
	}
}
