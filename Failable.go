package policy

// Failable is the core type in the policy class.  A Failable represents a
// function that can fail to complete, returning an error.  A Failable is
// considered to have succeeded when it returns with a nil error.
type Failable func() error
